const express=require('express')
const model=require('./model')
const utils=require('utility')
const User=model.getModel('user')
const Chat=model.getModel('chat')
const Router=express.Router()
const _filter={pwd:0,__v:0}

//获取登录信息
Router.get('/info',function (req,res) {
    const {userid}=req.cookies
    if(!userid){
        return res.json({code:1})
    }
    User.findOne({_id:userid},_filter,function (err,doc) {
        if(err){
            return res.json({code:1,msg:'后端出错了'})
        }
        if(doc){
            return res.json({code:0,data:doc})
        }
    })
})

//获取用户列表
Router.get('/list',function(req,res){
    // UserInfo.remove({},function (err,doc) {
    //
    // })
    // Chat.remove({},function (err,doc) {
    //
    // })
    const {type}=req.query
    User.find({type},function (err,doc) {
        if(err){
            return res.json({code:1,msg:'获取列表出错'})
        }
        return res.json({code:0,data:doc})
    })
})

//更新信息
Router.post('/update',function (req,res) {
    const {userid}=req.cookies
    if(!userid){
        return res.json({code:1,msg:"登录信息已过期"})
    }
    const body=req.body
    console.log(body)
    User.findByIdAndUpdate(userid,body,function (err,doc) {
        const data = Object.assign({},{
            user:doc.user,
            type:doc.type
        },body)
        return res.json({code:0,data})
    })


})
//登录
Router.post('/login',function (req,res) {
    const{user,pwd}=req.body
    User.findOne({user,pwd:md5Pwd(pwd)},_filter,function (err,doc) {
        if(!doc){
            return res.json({code:1,msg:'用户名或密码错误'})
        }
        res.cookie('userid',doc._id)
        return res.json({code:0,data:doc})
    })

})

//注册
Router.post('/register',function (req,res) {
    const{user,pwd,type}=req.body
    User.findOne({user},function (err,doc) {
        if(doc){
            return res.json({code:1,msg:'用户名重复'})
        }
        const userModel = new User({user,type,pwd:md5Pwd(pwd)})
        userModel.save(function (err,doc) {
            if(err){
                return res.json({code:1,msg:'后端出错了'})
            }
            const{user,type,_id}=doc
            res.cookie('userid',_id)
            return res.json({code:0,data:{user,type,_id}})
        })
    })

})

//获取所有聊天信息
Router.get('/getmsglist',function(req,res){
    const {userid}=req.cookies
    User.find({},function(err,userdoc){
        let users={}
        userdoc.forEach(v=>{
            users[v._id]={name:v.user,avator:v.avator}
        })
        Chat.find({'$or':[{from:userid},{to:userid}]},function(err,doc){
            if(!err){
                return res.json({code:0,msgs:doc,users:users,userid:userid})
            }
        })
    })
})

//更改聊天已读未读状态
Router.post('/updateChat',function(req,res){
    const {userid}=req.cookies
    const {from}=req.body
    Chat.update({from:from,to:userid},{'$set':{read:true}},{'multi':true},function (err,doc) {
        if(err){
            return res.json({code:1,msg:'修改失败'})
        }
        return res.json({code:0,from:from,num:doc.nModified})
    })


})


function md5Pwd(pwd){
    const salt='wtyiswty'
    return utils.md5(utils.md5(pwd+salt))
}
module.exports=Router