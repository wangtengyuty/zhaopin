const express=require('express');
const mongoose=require('mongoose');
const app=express();

//路由，get，put，post，delete都有
app.get('/',function (req,res) {
    res.send('Hello world')
});
app.get('/wty/a',function(req,res,next){
    console.log("response will be send by the next function")
    next();
},function(req,res){
    res.send("wty b")
})

app.get("/data",function(req,res){
    myModel.find({},function(err,doc){
        res.json(doc)
    })
})

app.use(function(req,res){
    console.log("中间件")
})

//使用静态资源 或者路由有多个模块，引入其他模块的路由
app.use(express.static('../public'))//使用静态资源
app.use("/static",express.static('../public'))//给静态资源一个虚拟路径


//处理404
app.use(function(req,res,next){
    res.status(404).send("404错误未找到此资源")
})
app.listen(9093,function(){
    console.log("test success");
});



//mongoDB

// setTimeout(()=>{
//     mongoose.disconnect(()=>console.log("断开连接"))
// },10000)

//Schema 定义MongoDB中集合Collection里文档document的结构
let mySchema=new mongoose.Schema({
    name:String,
    age:Number
})

mySchema.add({
    scope:Number
})


//Model 根据Schema编译出的构造器，或者称为类，通过Model实例化document
//document的创建和检索都需要通过模型model来处理
//返回值和第一个参数最好相同
let myModel=mongoose.model('myModel',mySchema)

//实例化文档-改进链接
//连接与断开

mongoose.connect("mongodb://localhost/db1",function(err){
    if(err){
        console.log('连接失败')
    }else{
        console.log('连接成功')
        //create
        // myModel.create({name:'wty'},{name:'lyc'},{name:'ynd'},function(err,doc1,doc2,doc3){
        //     console.log(doc1)
        //     console.log(doc2)
        //     console.log(doc3)
        // })


        // var doc1=new myModel({name:'wty',age:'23',scope:'100'})
        // doc1.save(function(err,doc){
        //     console.log(doc)
        // })
    }
});

