const mongoose = require('mongoose')
const DB_URL = "mongodb://127.0.0.1:27017/zhaopin"
mongoose.connect(DB_URL, function (err) {
    if (err) {
        console.log("连接失败")
    } else {
        console.log("连接成功")
    }
})

//schema定义文档结构，通过schema创建model操作文档

const models={
    user:{
        'user':{type:String,require:true},
        'pwd':{type:String,require:true},
        'type':{type:String,require:true},
        //头像
        'avator':{type:String},
        //描述 招聘职位或者求职意向
        'desc':{type:String},
        'title':{type:String},
        //如果是boss 公司和可以给出的薪资
        'company':{type:String},
        'money':{type:String}
    },
    chat:{
        'chatid':{type:String},
        'from':{type:String,require:true},
        'to':{type:String,require:true},
        'read':{type:Boolean,default:false},
        'content':{type:String,require:true,default:''},
        'create_time':{type:Number,default:new Date().getTime()}
    }
}


for(let m in models){
    mongoose.model(m,new mongoose.Schema(models[m]))
}


module.exports={
    getModel:function(name){
        return mongoose.model(name)
    }
}