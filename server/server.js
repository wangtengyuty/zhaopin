const express = require('express')
const http=require('http')
const userRouter = require('./user')
const bodyParser=require('body-parser')
const cookieParser=require('cookie-parser')
const model=require('./model')
const Chat=model.getModel('chat')

const app = express()
const server=http.Server(app)
const io=require('socket.io')(server)

io.on('connection',function(socket){
    socket.on('sendMessage',function (data) {
        const{from,to,content}=data
        const chatid=[from,to].sort().join("_")
        Chat.create({chatid,from,to,content,create_time:new Date().getTime()},function(err,doc){
            io.emit("receiveMessage",doc)
        })
    })
})


app.use(bodyParser.json())
app.use(cookieParser())
app.use('/user',userRouter)
server.listen(9093, function () {
    console.log("node app start at port 9093")
})

