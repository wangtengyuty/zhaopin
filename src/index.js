import React from 'react'
import ReactDom from 'react-dom'
import {Provider} from 'react-redux'
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import './config'
import './index.css'
import Login from "./container/login/login";
import Register from "./container/register/register";
import AutoRoute from "./component/authroute/authRoute";
import store from './redux/store'
import BossInfo from "./container/bossinfo/bossinfo";
import NiurenInfo from "./container/niureninfo/niureninfo";
import DashBoard from "./component/dashboard/dashboard";
import Chat from "./component/chat/chat";
ReactDom.render(
    (<Provider store={store}>
        <BrowserRouter>
            <div>
                <AutoRoute/>
                <Switch>
                    <Route exact path='/' component={Login}/>
                    <Route path={'/niureninfo'} component={NiurenInfo} ></Route>
                    <Route path={'/bossinfo'} component={BossInfo}></Route>
                    <Route path='/login' component={Login}></Route>
                    <Route path='/register' component={Register}></Route>
                    <Route path={'/chat/:id'} component={Chat}></Route>
                    <Route component={DashBoard}></Route>
                </Switch>
            </div>
        </BrowserRouter>
    </Provider>),
    document.getElementById('root')
)