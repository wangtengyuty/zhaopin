import React from 'react'
import axios from 'axios'
import {withRouter} from 'react-router-dom'
import {connect} from 'react-redux'
import {loadData} from "../../redux/actions/userActions";

@connect(
    null,
    {loadData}
)
@withRouter
class AuthRoute extends React.Component{

    componentDidMount(){
        //获取用户信息
        const publicList=['/login','/register' ]
        const pathname=this.props.location.pathname
        if(publicList.indexOf(pathname)!==-1){
            return null;
        }
        axios.get("/user/info").then(res=>{
            if(res.status===200){
                if(res.data.code===0){
                    this.props.loadData(res.data.data)
                }else{
                    this.props.history.push('/login')
                }
            }
        })
        // 是否登录
        // url是否是login 是的话无需跳转，不是的话需要跳转
        // 是牛人还是boss
        // 用户是否完善信息
    }

    render(){
        return null
    }

}

export default AuthRoute