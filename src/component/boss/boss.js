import React from 'react'
import {connect} from 'react-redux'
import {getUserList} from "../../redux/actions/chatUserActions";
import UserCard from "../usecard/usercard";

@connect(
    state=>state.chatuser,
    {getUserList}
)
class Boss extends React.Component{

    componentDidMount(){
        this.props.getUserList('niuren')
    }


    render(){
        return(
            <div>
                <UserCard userlist={this.props.userlist}></UserCard>
            </div>
        )
    }
}

export default Boss