import React from 'react'
import {connect} from 'react-redux'
import {List, Result, WhiteSpace, Modal} from 'antd-mobile'
import {logoutSubmit,toUpdateInfo} from "../../redux/actions/userActions";

const cookies=require('browser-cookies')
@connect(
    state => state.user,
    {logoutSubmit,toUpdateInfo}
)


class UserInfo extends React.Component {
    constructor(props) {
        super(props)
        this.logout = this.logout.bind(this)
        this.updateInfo=this.updateInfo.bind(this)
    }

    logout() {
        Modal.alert('登出','确定退出',[
            { text: '取消', onPress: () => console.log('cancel'), style: 'default' },
            { text: '退出', onPress: () => {
                cookies.erase('userid')
                this.props.logoutSubmit()
                } },
        ])
    }


    updateInfo(){
        this.props.toUpdateInfo()
        //获取用户种类用于获取url
        const type=this.props.type;
        let url=(type==='boss')?'/bossinfo':'niureninfo'
        console.log(url)
        this.props.history.push(url)
    }

    render() {
        const Item = List.Item
        const Brief = Item.Brief
        const props = this.props
        return props.user ?
            <div>
                <Result
                    img={<img src={require(`../img/${props.avator}.png`)} style={{width: 50}} alt=""/>}
                    title={props.user}
                    message={props.type === 'boss' ? props.company : null}
                />
                <WhiteSpace/>
                <List renderHeader={() => '简介'}>
                    <Item>
                        {props.title}
                        {props.desc.split('\n').map(v => <Brief key={v}>招聘需求:{v}</Brief>)}
                        {props.money ? <Brief>薪资:{props.money}</Brief> : null}
                    </Item>
                </List>
                <List>
                    <Item onClick={this.updateInfo}>修改信息</Item>
                    <Item onClick={this.logout}>退出登录</Item>
                </List>
                <WhiteSpace/>
            </div> :null
            // <Redirect to={props.redirectTo}/>

    }
}

export default UserInfo

