import React from 'react'
import {NavBar} from 'antd-mobile'
import NavLink from "../navlink/navlink";
import {connect} from 'react-redux'
import {Route, Switch} from 'react-router-dom'
import Boss from "../boss/boss";
import Niuren from "../niuren/niuren";
import Msg from "../msg/msg";
import UserInfo from "../user/userinfo";
import {getMsgList, receiveMessage, onSocket} from "../../redux/actions/chatAction";

@connect(
    state => state,
    {getMsgList, receiveMessage, onSocket}
)
class DashBoard extends React.Component {

    componentDidMount() {
        this.props.getMsgList()
        if (!this.props.chat.onSocket) {
            this.props.receiveMessage()
            this.props.onSocket()
        }

    }


    render() {
        const badge = this.props.chat.unread
        const pathname = this.props.location.pathname
        const user = this.props.user
        const navlist = [
            {
                path: '/boss',
                title: '牛人列表',
                text: '牛人',
                component: Boss,
                icon: 'boss',
                hide: user.type === 'niuren',//根据登录用户来判断是否隐藏
            },
            {
                path: '/niuren',
                title: 'Boss列表',
                text: 'Boss',
                component: Niuren,
                icon: 'job',
                hide: user.type === 'boss',
            },
            {
                path: '/msg',
                title: '消息列表',
                text: '消息',
                component: Msg,
                icon: 'msg',
                hide: false,
            },
            {
                path: '/me',
                title: '个人中心',
                text: '我',
                component: UserInfo,
                icon: 'user',
                hide: false,
            }

        ]
        //随意的一个路径匹配不上会报一个空的错误
        const title=navlist.find(v => v.path === pathname)?navlist.find(v => v.path === pathname).title:''

        return (
            <div>
                <NavBar className={'fixd-header'} mode="dard">{title}</NavBar>
                <div style={{marginTop: 45}}>
                    <Switch>
                        {navlist.map(v => (
                            <Route key={v.path} path={v.path} component={v.component}></Route>
                        ))}
                    </Switch>
                </div>
                <NavLink data={navlist} badge={badge}></NavLink>
            </div>
        )
    }


}


export default DashBoard