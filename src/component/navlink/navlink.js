import React from 'react'
import {TabBar} from 'antd-mobile'
import {withRouter}from 'react-router-dom'

@withRouter
class NavLink extends React.Component {
    render() {
        // 过滤掉hide为true的
        // 那就是取得hide为false的
        //const navList = this.props.data.filter(v=>!v.hide) 这种不用配置hide 可能v.hide不存在直接就默认false了
        const navList=this.props.data.filter(v=>v.hide===false)
        const Item = TabBar.Item
        const {pathname}=this.props.location
        return (
                <div className={'am-tab-bar'}>
                <TabBar >
                {navList.map(v=>(
                    <Item
                        badge={v.path==='/msg'?this.props.badge:null}
                        key={v.path}
                        title={v.text}
                        icon={{uri:require(`./img/${v.icon}.png`)}}
                        selectedIcon={{uri:require(`./img/${v.icon}-active.png`)}}
                        selected={v.path===pathname}
                        onPress={()=>{
                            this.props.history.push(v.path)
                        }}
                    >
                    </Item>
                ))}
                </TabBar>
                </div>
        )
    }
}

export default NavLink