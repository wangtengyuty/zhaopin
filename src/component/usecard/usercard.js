import React from 'react'
import {Card, WingBlank, WhiteSpace} from 'antd-mobile';
import {withRouter} from 'react-router-dom'

@withRouter
class UserCard extends React.Component {

    render() {

        const userlist = this.props.userlist
        const Body = Card.Body
        const Header = Card.Header
        const Footer = Card.Footer
        // console.log(userlist)
        return (
            <div>
                <WingBlank>
                    <WhiteSpace></WhiteSpace>
                    {userlist.map(user => (
                            user.avator ? (
                                <div　key={user._id} onClick={()=>this.props.history.push(`/chat/${user._id}`)}>
                                    <Card >
                                        <Header
                                            title={user.user}
                                            thumb={require(`../img/${user.avator}.png`)}
                                            extra={user.title}
                                        />
                                        <Body>
                                        {/*{user.type==='boss'?'职位需求: ':'寻求职位: '}{user.title}<br/>*/}
                                        {user.type === 'boss' ? '职位要求:' : '个人技能'}
                                        {user.desc.split('\n').map(d => (
                                            <div key={d}>{d}</div>
                                        ))}
                                        </Body>
                                        <Footer content={user.type === 'boss' ? <div>{`薪资:${user.money}`}</div> :
                                            <div>{`期望薪资:${user.money}`}</div>}
                                                extra={(user.type === 'boss' ? `公司:${user.company}` : null)}/>

                                    </Card>
                                    <WhiteSpace/>
                                </div>
                            ) : null
                        )
                    )}
                </WingBlank>
            </div>
        )
    }
}


export default UserCard