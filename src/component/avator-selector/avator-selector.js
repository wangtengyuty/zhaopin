import React from 'react'
import {Grid, List} from 'antd-mobile'
import {connect} from 'react-redux'

@connect(
    state=>state.user
)
class AvatarSelector extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            icon:this.props.avator?require(`../img/${this.props.avator}.png`):'',
            text:this.props.avator?this.props.avator:''
        }
    }

    render() {


        const avatorList = 'boy,girl,man,woman,bull,chick,crab,hedgehog,hippopotamus,koala,lemur,pig,tiger,whale,zebra'.split(',')
            .map(
                v => ({
                    icon: require(`../img/${v}.png`),
                    text: v
                })
            )



        const girdHeader = (this.state.icon!=='') ?
            (<div>
                <span>已选择头像</span>
                <img style={{width:20}}  src={this.state.icon} alt=""></img>
            </div>)
            : '请选择头像'

        return (
            <div>
                <List renderHeader={()=>girdHeader}>
                    <Grid data={avatorList} columnNum={5}
                        onClick={(element)=>{
                            this.setState({
                                icon:element.icon,
                                text:element.text
                            })
                            this.props.selectAvatar(element.text)
                        }}
                    />
                </List>
            </div>
        )
    }
}


export default AvatarSelector