import React from 'react'
import {InputItem} from 'antd-mobile'
import {connect} from 'react-redux'
import {NavBar, List, Grid} from 'antd-mobile';
import {sendMessage, getMsgList, receiveMessage, onSocket,readMessage} from "../../redux/actions/chatAction";
import {getChatId} from "../../redux/util";
import '../../index.css'

const Item = List.Item

@connect(
    state => state,
    {sendMessage, getMsgList, receiveMessage, onSocket,readMessage}
)
class Chat extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            text: "",
            msg: [],
            showEmoji: false
        }
        this.handleSubmit = this.handleSubmit.bind(this)
    }


    handleSubmit() {
        const from = this.props.user._id
        const to = this.props.match.params.id
        const content = this.state.text
        if (content === '') {
            return;
        }
        this.props.sendMessage({from: from, to: to, content: content})
        this.setState({text: ''})
    }

    fixCarousel() {
        setTimeout(function () {
            window.dispatchEvent(new Event('resize'))
        }, 0)
    }

    componentDidMount() {
        this.props.getMsgList()
        if (!this.props.chat.onSocket) {
            this.props.receiveMessage()
            this.props.onSocket()
        }

        this.fixCarousel()
    }


    componentWillUnmount(){
        //更改已读 更新redux中的状态 并且更改与当前用户聊天的这条信息 from对方的/to自己的 unread更改为true
        const from=this.props.match.params.id
        this.props.readMessage({from:from})
    }


    /*<List>
    <Item>Title</Item>
    <Item extra="10:30" align="top" thumb="https://zos.alipayobjects.com/rmsportal/dNuvNrtqUztHCwM.png" multipleLine>
    Title <Brief>subtitle</Brief>
    </Item>
    </List>
      */


    render() {

        const from = this.props.user._id
        const to = this.props.match.params.id
        const chatId = getChatId(from, to)
        const userid = this.props.match.params.id;
        const users = this.props.chat.users

        //emoji
        const emoji = '😀 😁 😂 🤣 😃 😄 😅 😆 😉 😊 😋 😎 😍 😘 😗 😙 😚 😐 😑 😶 🙄 😏 😣 😥 😮 🤐 😯 😪 😫 😴 😌 😛 😜 😝 🤤 😒 😓 😔 😕 🙃 🤑 😲 ☹️ 🙁 😖 😞 😟 😤 😢 😭 😦 😧 😨 😩 😬 😰 😱 😳 😵 😡 😠 😷 🤒 🤕 🤢 🤧 😇'
            .split(' ').map(v => ({text: v}))


        if (!users[userid]) {
            return null;
        }
        const msg = this.props.chat.chatMsg.map(v => {
            const avator = require(`../img/${users[v.from].avator}.png`)
            if (chatId === v.chatid) {
                return v.to === this.props.user._id ?
                    (<List key={v._id}>
                        <Item wrap
                              thumb={avator}>
                            {v.content}
                        </Item>
                    </List>) :
                    (<List key={v._id}>
                        <Item wrap
                              className='chat-me'
                              extra={<img src={avator} alt={''}/>}
                        >
                            {v.content}
                        </Item>
                    </List>)


            } else {
                return null
            }
        })
        return (
            <div id={'chat-page'}>
                <NavBar className={'fixd-header'}
                        mode="dark"
                        leftContent="Back"
                        onLeftClick={() => this.props.history.goBack(-1)}
                >{users[userid].name}</NavBar>

                <div style={{marginTop: 45}}>
                    {msg}
                </div>


                <div className={'stcik-footer'}>
                    <List>
                        <InputItem
                            placeholder={'请输入'}
                            value={this.state.text}
                            onChange={(v) => this.setState({
                                text: v
                            })}
                            extra={
                                <div>
                                    <span style={{marginRight: 15}}
                                          onClick={() => {
                                              this.setState({
                                                  showEmoji: !this.state.showEmoji
                                              })
                                              this.fixCarousel()
                                          }}
                                    >😀</span>
                                    <span onClick={this.handleSubmit}>发送</span>
                                </div>
                            }
                        />
                    </List>
                    {this.state.showEmoji ? <Grid
                        data={emoji}
                        activeStyle={false}
                        columnNum={9}
                        carouselMaxRow={3}
                        isCarousel={true}
                        onClick={(el) => {
                            this.setState({
                                text: this.state.text + el.text
                            })
                        }}
                    /> : null}
                </div>

            </div>


        )
    }
}

export default Chat