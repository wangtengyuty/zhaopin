


export function getRedirectPath({type,avator}) {
    //根据用户信息返回跳转地址
    let url= (type==='boss')?'/boss':'niuren'
    if(!avator){
        url +='info'
    }
    return url
}


export function getChatId(from,to){
    return [from,to].sort().join("_")
}