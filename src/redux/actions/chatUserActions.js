import axios from 'axios'



export const USER_LIST='USER_LIST'
export const ERROR_LIST='ERROR_LIST'

function userList(data){
    return {
        type: USER_LIST,
        payload:data
    }
}


function errorList(msg){
    return {
        type:ERROR_LIST,
        payload:msg
    }
}

export function getUserList(type){
    return dispatch=>{
        axios.get('/user/list?type='+type)
            .then(res=>{
                if(res.status===200&&res.data.code===0){
                    dispatch(userList(res.data.data))
                }else{
                    dispatch(errorList(res.data.msg))
                }
            })
    }
}