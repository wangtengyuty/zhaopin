import {combineReducers} from 'redux'
import {user} from './userReducer'
import {chatuser} from "./chatUserReducer";
import {chat} from "./chatReducer";

const allReducers={
    user:user,
    chatuser:chatuser,
    chat:chat

}

const rootReducer=combineReducers(allReducers)

export default rootReducer;