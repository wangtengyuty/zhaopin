import {USER_LIST, ERROR_LIST} from '../actions/chatUserActions'

const initialState = {
    userlist: []
}


export function chatuser(state = initialState, action) {
    switch (action.type) {
        case USER_LIST:
            return {...state, userlist: action.payload}
        case ERROR_LIST:
            return {...state, msg: action.payload}
        default:
            return state
    }
}