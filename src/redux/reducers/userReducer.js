import {AUTH_SUCCESS,ERROR_MSG,TO_REGISTRY,LOAD_DATA,LOGOUT,TO_UPDATEINFO} from '../actions/userActions'
import {getRedirectPath} from '../util'

const initialState = {
    redirectTo: '',
    msg: '',
    user: '',
    type: ''
}


export const user = function (state = initialState, action) {
    switch (action.type) {
        case AUTH_SUCCESS:
            return {...state, msg:'', redirectTo: getRedirectPath(action.payload), ...action.payload}
        case ERROR_MSG:
            return {...state, msg: action.msg}
        case TO_REGISTRY:
            return{...state,...action.payload}
        case LOAD_DATA:
            return{...state,...action.payload}
        case LOGOUT:
            return{...initialState,redirectTo:'/login'}
        case TO_UPDATEINFO:
            return{...state,redirectTo:''}
        default:
            return state;
    }
}