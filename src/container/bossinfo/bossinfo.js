import React from 'react'
import {NavBar, InputItem, TextareaItem,Button} from 'antd-mobile'
import AvatarSelector from "../../component/avator-selector/avator-selector";
import {connect} from 'react-redux'
import {update} from "../../redux/actions/userActions";
import {Redirect} from 'react-router-dom'

@connect(
    state=>state.user,
    {update}
)
class BossInfo extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            title: this.props.title?this.props.title:'',
            desc: this.props.desc?this.props.desc:'',
            company: this.props.company?this.props.company:'',
            money: this.props.money?this.props.money:'',
            avator: this.props.avator?this.props.avator:''
        }
        this.selectAvatar=this.selectAvatar.bind(this)
    }


    handleChange(key, value) {
        this.setState({
            [key]: value
        })
    }

    selectAvatar(imgName){
        this.setState({
            avator:imgName
        })
    }

    render() {
        const path=this.props.location.pathname
        const redirect=this.props.redirectTo

        //招聘职位
        const title=this.props.title
        //公司
        const company=this.props.company
        //薪资
        const money=this.props.money
        //职位要求
        const desc=this.props.desc

        return (
            <div id={'info'}>
                {(redirect&&redirect!==path)?<Redirect to={this.props.redirectTo}/>:null}
                <NavBar>Boss信息完善</NavBar>
                <AvatarSelector selectAvatar={this.selectAvatar}></AvatarSelector>
                <InputItem onChange={(v) => (this.handleChange('title', v))} defaultValue={title?title:''}>招聘职位:</InputItem>
                <InputItem onChange={(v) => (this.handleChange('company', v))} defaultValue={company?company:''}>公司:</InputItem>
                <InputItem onChange={(v) => (this.handleChange('money', v))} defaultValue={money?money:''}>薪资:</InputItem>
                <TextareaItem
                    defaultValue={desc?desc:''}
                    onChange={(v) => this.handleChange('desc', v)}
                    title={'职位要求:'}
                    rows={5}
                    // autoHeight
                ></TextareaItem>
                <Button type={'primary'} onClick={()=>this.props.update(this.state)}>提交</Button>
            </div>
        )
    }
}

export default BossInfo