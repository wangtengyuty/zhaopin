import React from 'react'
import Logo from "../../component/logo/logo";
import {Button, InputItem, List, WhiteSpace, WingBlank} from 'antd-mobile'
import {Redirect} from 'react-router-dom'
import {connect} from 'react-redux'
import {login, toRegistry} from '../../redux/actions/userActions'

@connect(
    state=>state.user,
    {login,toRegistry}
)
class Login extends React.Component {
    constructor(props){
        super(props)
        this.state={
            user: '',
            pwd: ''
        }
        this.registry=this.registry.bind(this)
        this.handleLogin = this.handleLogin.bind(this);
    }


    registry(){
        this.props.toRegistry();
        this.props.history.push("/register")
    }

    handleChange(key,value){
        this.setState({
            [key]:value
        })
    }

    handleLogin(){
        this.props.login(this.state)
    }
    render() {
        return (
            <div>
                {this.props.redirectTo?<Redirect to={this.props.redirectTo}/>:null}
                <Logo/>
                <h2>登录页</h2>
                {this.props.msg?<p className={'error-msg'}>{this.props.msg}</p>:null}
                <WingBlank>
                    <List>
                        <InputItem onChange={v=>this.handleChange('user',v)}>用户</InputItem>
                        <InputItem type={'password'} onChange={v=>this.handleChange('pwd',v)}>密码</InputItem>
                    </List>
                    <Button type={'primary'} onClick={this.handleLogin}>登录</Button>
                    <WhiteSpace/>
                    <Button type={'primary'} onClick={this.registry}>注册</Button>
                </WingBlank>
            </div>
        )
    }
}

export default Login