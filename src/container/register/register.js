import React from 'react'
import {Button, InputItem, List, Radio, WhiteSpace, WingBlank} from 'antd-mobile'
import Logo from "../../component/logo/logo";
import {register} from '../../redux/actions/userActions'
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom'


@connect(
    state=>state.user,
    {register}
)
class Register extends React.Component{
    constructor(props){
        super(props)
        this.state={
            user:'',
            pwd:'',
            repeatpwd:'',
            type:"niuren"
        }
        this.handleRegister=this.handleRegister.bind(this)
    }

    handleChange(key,value){
        this.setState({
            [key]:value
        })
    }

    handleRegister(){

        // console.log(this.props)
        // console.log(this.props.user)
        this.props.register(this.state)
    }

    render(){
        const RadioItem=Radio.RadioItem
        return(
            <div>
                {this.props.redirectTo?<Redirect to={this.props.redirectTo}/>:null}
                <Logo/>
                <WingBlank>
                    <h2>注册页</h2>
                    {this.props.msg?<p className={'error-msg'}>{this.props.msg}</p>:null}
                    <List>
                        <InputItem onChange={v=>this.handleChange('user',v)}>用户名</InputItem>
                        <InputItem type={"password"} onChange={v=>this.handleChange('pwd',v)}>密码</InputItem>
                        <InputItem type={"password"} onChange={v=>this.handleChange('repeatpwd',v)}>确认密码</InputItem>
                        <RadioItem
                            checked={this.state.type==='niuren'}
                            onChange={()=>this.handleChange("type","niuren")}
                        >牛人</RadioItem>
                        <RadioItem
                            checked={this.state.type==='boss'}
                            onChange={()=>this.handleChange("type","boss")}
                        >Boss</RadioItem>
                        <Button type={'primary'} onClick={this.handleRegister}>注册</Button>
                        <WhiteSpace/>
                        <Button type={'primary'} onClick={()=>this.props.history.goBack()}>返回</Button>
                    </List>
                </WingBlank>
            </div>
        )
    }
}

export default Register